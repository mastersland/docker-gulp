# Docker Gulp

A small Docker image with **gulp** based on [Node.js Alpine image](https://hub.docker.com/_/node/).  


## Examples

### Run a single gulp command
```console
docker run -it --rm --name my-running-script -v "$PWD":/workdir -w /workdir mastersland/docker-gulp:latest gulp watch
```

### Usage in docker-compose.yml
```yaml
  gulp:
    image: mastersland/docker-gulp:latest
    restart: "always"
    volumes:
      - /path/to/theme:/workdir
```